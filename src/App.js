import React, {Component} from 'react';
import Square from './components/Square';
import ResetButton from './components/ResetButton';
import Tries from "./components/Tries";

import './App.css';

class App extends Component {

    makeDivs = () => {
        let Array = [];
        for (let i = 0; i < 36; i++) {
            Array.push({type: <Square />, className: 'Inactive', hasItem: false, letter: ""});
        }
        let random = Math.floor(Math.random() * (35));
        Array[random].hasItem = true;
        Array[random].letter = "0";
        return Array;
    };

    state = {
        tries: 0,
        Array: this.makeDivs(),

    };

    changeClass = (id) => {
        let tries = this.state.tries;
        tries++;
        let Array = [...this.state.Array];
        let square = Array[id];
        square.className = "Active";
        if(square.hasItem === true){
            alert('You win from ' + tries + " try");
        }
        Array[id] = square;
        this.setState({tries, Array});
    };

    resetPage = () => {
        let tries = 0;
        let Array = this.makeDivs();
        this.setState({Array, tries});
    };

    render() {
        console.log(this.state.Array);
        let square = this.state.Array.map((square, id) => (
            <Square
                key={id}
                className={square.className}
                onClick={() => this.changeClass(id)}
                letter={square.letter}
            >
            </Square>
        ));
        return (
            <div className="App">
                <div className="Wrap">
                    {square}
                </div>
                <Tries tries={this.state.tries} />
                <ResetButton reset={() => this.resetPage()} />
            </div>
        );
    }
}

export default App;
