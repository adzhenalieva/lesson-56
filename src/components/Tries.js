import React from "react";

const Tries = (props) => {
    return(
        <div className="NextWrap">
        <p className="Tries">Tries: {props.tries}</p>
        </div>
    )
};

export default Tries