import React from "react";

const ResetButton = (props) => {
    return (
        <button className="ResetButton" onClick={props.reset}>Reset</button>
    )
};

export default ResetButton
