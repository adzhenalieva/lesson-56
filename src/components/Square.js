import React from "react";

const Square = props => {
    return (
        <div className={props.className} onClick={props.onClick}><i>{props.letter}</i></div>
    )
};

export default Square